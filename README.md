# Ransomware


Code that I make for testing / education please be careful in the use of the code above.

** RISK IN YOUR OWN RISK, CODE ABUSE ABOVE OUTSIDE MY RESPONSIBILITY and Anyone who is harmed by this open source, I am not responsible for this open source abuse. **

> Note : Unfortunatelly now some antiviruses (including Windows Defender) detects the unlocker as a virus. Disable any antivirus to play with the project.

**Remember, security is always a double-edged sword**

Demo video 
<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="ransomware.png">
    <source src="demo.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->

### What is Ransomware?

Ransomware is a type of malware that prevents or limits users from accessing their system, either by locking the system's screen or by locking the users' files unless a ransom is paid. More modern ransomware families, collectively categorized as crypto-ransomware, encrypt certain file types on infected systems and forces users to pay the ransom through certain online payment methods to get a decrypt key.

### Project Summary

This project was developed for the Computer Security course at my academic degree. Basically, it will encrypt your files in background using AES-128-CBC symetric key.

This project is composed of two parts : The server and the ransomware code.

The server store the AES symetric key and hosts a website with the countdown for giving bitcoins.

The ransomware contact the webserver to get the AES key and encrypts with it all files in Documents, Downloads and Desktop directory. After all the process is done, the ransomware open a web browser on the hacker website.

## Setting up the project Step by Step 

In order to reproduce the project, we will use two machines, a linux VM (Ubuntu) which will be the server and a windows 10 VM.

For the sake of simplicity, I have a folder mapped to the VM, so I can compile from my linux and copy to the vm.
### Website configuration 
Install apache2 et delete the default site config file. Create a new config file for the hacker website and set th DocumentRoot parameter to : ```/var/www/crypto```
crypto is the folder which need to be in ```/var/www/```.
Generate the symetric key AES-128-CBC in file like ```key.key```
```
openssl enc -aes-128-cbc -k secret -P -iter 10 > key.key
```
Then, copy the KEY field in : ```/var/www/crypto/register.php``` in the variable ```$encryption_key```
And install PHP (apt install php) and check if index.php exists :
```
/etc/apache2/mods-available & mods-enabled/dir.conf
```
Otherwise add it in the list
Activate the website (a2ensite …) and restart the apache2 service :
```
sudo systemctl restart apache2 && systemctl enable apache2
```

## Windows 10 Configuration
You will need to install python (pip will be installed in the same time) and you're gonna install some python3 modules with pip install
in a powershell terminal :
	• request (for http requests)
	• cryptography (in order to crypt)
For the demo install some files like (pdf, docx, png..) in Documents, Desktop and Downloads because the script works in these directories.
### Launch the attack
execute the script in windows terminal like powershell or cmd
```
python ransomware.py
```
You will that your files has been encrypted.